# Script for automatic cleanup of the DNS slave servers with a DirectAdmin as master.
#
# The script remove old DNS Zones that no longer exists on the DirectAdmin master.
# This script is only for slave servers with DirectAdmin as master
#
# @author Jeroen Spaans <jeroen@tomorrowsit.nl>
# @package slavedns_cleanup
# @version 0.1
#
# 0.1
# Initial Release of the script

import settings
import logging
import time
import subprocess
from os import path, listdir, remove
from modules.da.da_api import API

logging.basicConfig(
    level=settings.log_level,
    format='%(asctime)s %(levelname)s %(message)s',
    handlers=[
        logging.FileHandler(path.join(settings.log_folder, time.strftime("%Y%m%d-%H%M%S") + '.log')),
    ])


def cleanup_slave_dns():
    logging.debug('Declare local dnszones array')
    dns_zones_local = []
    dns_zones_not_exists = []

    logging.info('Get DNS Zones from cache')
    for zone in sorted(listdir(settings.bind_zone_path)):
        if zone not in settings.bind_excluded_zones:
            logging.debug('Add domain to array: ' + zone)
            dns_zones_local.append(zone)
        else:
            logging.info('Zone not added to array (EXCLUDED): ' + zone)

    if dns_zones_local:
        logging.info('Total domains in array: ' + str(len(dns_zones_local)))
        logging.info('Start check if domains exists in DirectAdmin')
        api = API(
            username=settings.da_username,
            password=settings.da_password,
            server=settings.da_url,
            debug=settings.da_debug,
            json=True
        )

        for zone in dns_zones_local:
            try:
                exists = api.CMD_API_DNS_ADMIN(
                    action='exists',
                    domain=zone
                )

                if exists['exists'] == "0":
                    logging.info('Domain not exists in DirectAdmin: ' + zone)
                    dns_zones_not_exists.append(zone)
                    logging.info('Domain added to array for deletion: ' + zone)

                else:
                    logging.debug('Domain exists in DirectAdmin: ' + zone)

            except:
                logging.error('Connection to DirectAdmin Failed')

        logging.info('Check in DirectAdmin finished')
        logging.info('Total DNS Zones marked for deletion: ' + str(len(dns_zones_not_exists)))

        if not settings.dryrun:
            if dns_zones_not_exists:
                logging.info('Start cleanup marked DNS Zones')
                for zone in dns_zones_not_exists:
                    try:
                        delete_zone = subprocess.run(["rndc", "delzone", zone], capture_output=True, text=True)
                        logging.debug('rndc output: ' + str(delete_zone.stdout))
                        if delete_zone.returncode == 0:
                            logging.info('DNS Zone deleted: ' + zone)
                            logging.debug('Output RNDC: ' + str(delete_zone.stdout))
                        elif delete_zone.returncode == 1 and 'not found' in delete_zone.stderr:
                            logging.warning('DNS Zone not found by RNDC ' + zone)
                            logging.debug('Output RNDC: ' + str(delete_zone.stderr))
                            logging.info('DNS Zone file will be deleted for zone: ' + zone)

                            zone_filepath = path.join(settings.bind_zone_path, zone)
                            logging.debug('Path for zone file to be deleted: ' + zone_filepath)

                            try:
                                remove(zone_filepath)
                                logging.info('Zone file deleted for zone: ' + zone)
                            except:
                                logging.error('Deletion of zone file FAILED for zone: ' + zone)
                        else:
                            logging.error('DNS Zone NOT deleted: ' + zone)
                            logging.error('Output RNDC: ' + str(delete_zone.stderr))
                    except:
                        logging.error('DNS Zone deletion FAILED: ' + zone)
                logging.info('Finished cleanup marked DNS Zones')
            else:
                logging.info("No domains marked for deletion")
        else:
            logging.info("DryRun activated, NOT going to do a real delete action")
    else:
        logging.warning('No domains in local array')


def cleanup_old_cleanup_logs():
    if settings.log_keep_files >= 1:
        logging.info('Count of logfiles to keep: ' + str(settings.log_keep_files))
        for logfile in sorted(listdir(settings.log_folder))[:-settings.log_keep_files]:
            if logfile.endswith('.log'):
                logging.info('Remove log file: ' + logfile)
                filename_relpath = path.join(settings.log_folder, logfile)
                remove(filename_relpath)
    else:
        logging.info('Log_keep_files is set to unlimited')


if __name__ == '__main__':
    logging.info('DNS Cleanup started')

    cleanup_slave_dns()

    logging.info('DNS Cleanup ended')
    logging.info('Start Cleanup old cleanup logs')

    cleanup_old_cleanup_logs()

    logging.info('Cleanup old cleanup logs ended')
