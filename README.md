# DirectAdmin Slave DNS Cleanup

Script for automatic cleanup of the DNS slave servers with a DirectAdmin as master.
The script remove old DNS Zones that no longer exists on the DirectAdmin master.

## Script steps
* Get all zone files on the slave server
* Check if zone file is in the exclusion list of the settings.py file
* Check if the zone exists on the DirectAdmin server by an API call to DirectAdmin
* Remove the zone bij rndc command if not exists
* Remove the zone file directly from the server if rndc command failes

## Install & Configure
The script must be installed on the slave dns server and need python to be installed.

### Create Login Key for DirectAdmin user with specific API permissions

Login keys feature must be enabled in DirectAdmin:

```bash
vi /usr/local/directadmin/conf/directadmin.conf
```

Add

```dotenv
login_keys=1
```

And in the user.conf file

```bash
/usr/local/directadmin/data/users/admin/user.conf
```

Add

```dotenv
login_keys=ON
```

Restart DirectAdmin

```bash
systemctl restart directadmin
```

Go the DirectAdmin portal and switch to the user section of the admin user

* Click on Login keys
* Create a key with only permissions for the CMD_API_DNS_ADMIN and only allowed from the server ip the script runs on.

### Install the script

#### Requirements
* git
* Python 3.8 or newer
* Python Requests (default installed on Ubuntu)
* Python SimpleJSON (default installed on Ubuntu)

#### Install

```bash
mkdir /apps/scripts
cd /apps/scripts
git clone https://<user>:<deploytoken>gitlab.com/digizoneict/script-library/da-slave-dns-cleanup.git
cd da-slave-dns-cleanup
```

#### Configure

```bash
cp settings.py.template settings.py
vi settings.py
```

Set the correct params in the settings.py file.

##### Debug modus
If you want debug logging you can set the following param in the settings.py file
`
log_level = logging.DEBUG
`

##### DryRun modus
If you want to run the script without realy remove the zones from the script but do only a log action, you can set the 
following param in the settings.py file.
`
dryrun = True
`

When the DryRun is set to True, the script runs normaly but only the delete action is disabled.

#### Create cron

```bash
crontab -e
```

Add the config below, with this cron the script runs daily on 23:00.

```cron
0 23 * * * /usr/bin/python3 /apps/scripts/da-slave-dns-cleanup/main.py >/dev/null 2>&1
```